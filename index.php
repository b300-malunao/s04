<?php require_once "./code.php" ?>

<?php
	$tasks = ["Get Git", "Bake HTML", "Eat CSS", "Learn PHP"];

	if(isset($_GET["index"])){
		$indexGet = $_GET["index"];
		echo "The retrieved task from GET is $tasks[$indexGet]. <br>";
	}

	if(isset($_POST["index"])){
		$indexPost  = $_POST["index"];
		echo "The retrieved task from POST is $tasks[$indexPost]. <br>";
	}

?>


<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S04 Activity </title>
</head>
<body>

	<!-- Supplementary Activity -->
		<h1>Task index from GET</h1>
		<form method="GET">
		    <select name="index" required>
		        <option value="0">0</option>
		        <option value="1">1</option>
		        <option value="2">2</option>
		        <option value="3">3</option>
		    </select>

		    <button type="submit">GET</button>
		</form>

		<h1>Task index from POST</h1>
		<form method="POST">
		    <select name="index" required>
		        <option value="0">0</option>
		        <option value="1">1</option>
		        <option value="2">2</option>
		        <option value="3">3</option>
		    </select>

		    <button type="submit">POST</button>
		</form>





	<!-- Activity -->

	<h1>Building</h1>
	<p>The name of the building is <?php echo $building->getName(); ?>.</p>
    <p>The <?php echo $building->getName(); ?> has <?php echo $building->getFloors(); ?> floors.</p>
    <p>The <?php echo $building->getName(); ?> is located at <?php echo $building->getAddress(); ?>.</p>
    <?php $building->setName("Caswynn Complex"); ?>
    <p>The name of the building has been changed to <?php echo $building->getName(); ?>.</p>

    <h1>Condominium</h1>
    <p>The name of the condominium is <?php echo $condominium->getName(); ?>.</p>
    <p>The <?php echo $condominium->getName(); ?> has <?php echo $condominium->getFloors(); ?> floors.</p>
    <p>The <?php echo $condominium->getName(); ?> is located at <?php echo $condominium->getAddress(); ?>.</p>
    <?php $condominium->setName("Enzo Tower"); ?>
    <p>The name of the condominium has been changed to <?php echo $condominium->getName(); ?>.</p>



</body>
</html>