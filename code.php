<?php

class Building {

	protected $name;
	protected $floors;
	protected $address;

	public function __construct($name, $floors, $address) {

		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;

	}



	public function getName() {

		return $this->name;
	}


	public function setName($name) {

		return $this->name = $name;
		
	}

	public function getFloors() {

		return $this->floors;
	}


	private function setFloors($floors) {

		return $this->floors = $floors;
		
	}


	public function getAddress() {

		return $this->address;
	}


	private function setAddress($address) {

		return $this->address = $address;
		
	}

}

class Condominium extends Building {
	public function __construct($name, $floors, $address) {
        parent::__construct($name, $floors, $address);
    }


	
}

$building = new Building("Caswyn Building", 8, "Timog Avenue, Quezon City, Philippines");
$condominium = new Condominium("Enzo Condo", 5, "Buendia Avenue, Makati City, Philippines");


// Supplementary Activity


$tasks = ["Get Git", "Bake HTML", "Eat CSS", "Learn PHP"];

    if(isset($_GET["index"])){
        $indexGet = $_GET["index"];
        echo "The retrieved task from GET is {$tasks[$indexGet]}. <br>";
    }

    if(isset($_POST["index"])){
        $indexPost = $_POST["index"];
        echo "The retrieved task from POST is {$tasks[$indexPost]}. <br>";
    }
